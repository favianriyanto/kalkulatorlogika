/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kalkulatorlogika;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 *
 * @author Favian Azwadt R
 */
public class Kalkulatorlogika extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int reply = JOptionPane.showConfirmDialog(null,
                    "Syarat dan Ketentuan penggunaan aplikasi ini\nSaya selaku pembuat tidak bertanggung jawab atas pemakaian aplikasi dalam waktu illegal seperti saat ujian dan hal lain sebagainya", "Baca dulu gan/sis", JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION){
            launch(args); 
        }
        else
        {
            System.exit(0);
        }
           
    }
    
}
