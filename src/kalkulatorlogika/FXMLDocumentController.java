/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kalkulatorlogika;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author Favian Azwadt R
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private TextField text1;
    @FXML
    private TextField text2;
    @FXML
    private TextField text4;
    @FXML
    private TextField text3;
    @FXML
    private TextArea test;
    

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void okebinarytohex(ActionEvent event) {
        test.clear();
        int digitNumber = 1;
            int sum = 0;
            String binary = text1.getText();
            for(int i = 0; i < binary.length(); i++){
                if(digitNumber == 1)
                    sum+=Integer.parseInt(binary.charAt(i) + "")*8;
                else if(digitNumber == 2)
                    sum+=Integer.parseInt(binary.charAt(i) + "")*4;
                else if(digitNumber == 3)
                    sum+=Integer.parseInt(binary.charAt(i) + "")*2;
                else if(digitNumber == 4 || i < binary.length()+1){
                    sum+=Integer.parseInt(binary.charAt(i) + "")*1;
                    digitNumber = 0;
                    if(sum < 10)
                        test.appendText(String.valueOf(sum));
                    else if(sum == 10)
                        test.appendText("A"); 
                    else if(sum == 11)
                        test.appendText("B");
                    else if(sum == 12)
                        test.appendText("C");
                    else if(sum == 13)
                        test.appendText("D");
                    else if(sum == 14)
                        test.appendText("E");
                    else if(sum == 15)
                        test.appendText("F");
                    sum=0;
                }
                digitNumber++;  
            }
            String kambing = test.getText();
            text2.setText(kambing);
    }

    @FXML
    private void okehextobinary(ActionEvent event) {
        String ayam = text3.getText();
        int num = (Integer.parseInt(ayam, 16));
        text4.setText(Integer.toBinaryString(num));
    }

    @FXML
    private void copy1(ActionEvent event) {
        StringSelection stringSelection = new StringSelection (text1.getText());
        Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard ();
        clpbrd.setContents (stringSelection, null);
    }

    @FXML
    private void paste1(ActionEvent event) throws UnsupportedFlavorException, IOException {
        String data = (String) Toolkit.getDefaultToolkit()
                .getSystemClipboard().getData(DataFlavor.stringFlavor);
        text1.setText(data);
    }

    @FXML
    private void copy3(ActionEvent event) {
        StringSelection stringSelection = new StringSelection (text3.getText());
        Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard ();
        clpbrd.setContents (stringSelection, null);
    }

    @FXML
    private void paste3(ActionEvent event) throws UnsupportedFlavorException, IOException {
        String data = (String) Toolkit.getDefaultToolkit()
                .getSystemClipboard().getData(DataFlavor.stringFlavor);
        text3.setText(data);
    }

    @FXML
    private void copy2(ActionEvent event) {
        StringSelection stringSelection = new StringSelection (text2.getText());
        Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard ();
        clpbrd.setContents (stringSelection, null);
    }

    @FXML
    private void copy4(ActionEvent event) {
        StringSelection stringSelection = new StringSelection (text4.getText());
        Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard ();
        clpbrd.setContents (stringSelection, null);
    }
    
}
